# version with two green terrain colors, "=>" in map description - Luciferor and Luigiii.

quality: below average
tooltips: poor
design changes: above average

- Loads of new heroes, including custom models.
- Water elemental creeps in reasonable position as creeps.
- Hidden heroes removed.
- Pots of invis added.
- Paths at center top and bot.


#  "[]SUPER[]" in title version - aaa90ii. "new heroes: try them".

quality: below average
tooltips: poor
design changes: below average

- Looks like 1 new hero and some hero/s replaced.
- Still has ramp on top and bottom.
- Still has invis pot.
- New hero.
- About average.
- Stupid title.
- It's misinformed to change things just for "freshness".
- Awful balance.
- Lich can summon 2 frost wyrms at level 1.


# "SUPER summoner..." in title.

quality: average
tooltips: average
design changes: average

- Seems very standard.
- Ramps at top and bottom.
- Like the basic Super summoner IG.


# "Kiro" version.

quality: below average
tooltips: above average
design changes: poor

oldest: lich is here, so based on aa90ii version maybe.
- Random, stupid creeps added, fighting at game start.
- Random items on the ground.
- Latest version: peak autism - auto spawning aos units, team owned blademasters holding position, "Kiro administrator hero".
- Not to mention stolen from aaa90ii.
- Demon archer.
- Loads of stupid items and new shops added. Global exp.
