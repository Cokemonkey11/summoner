# colored 3.x verison edited by The_magament

quality: above average
tooltips: maybe no change
design changes: average

- removed hidden heroes
- added a number of heroes
- item shops in center area
- quiet
- close to original
- flame strike, hex, fan of knives avail as permanent items.
- Hero called "mix summoner" a bit stupid.


# 6.x

quality: average
tooltips: above average
design changes: average

- 6.3beta: classic playthesummoner@yahoo.com, looks like the original and original items.
- new hidden hero: nerubian summoner.
- custom icon for his "spider lords" summon.
- 6.4: same as 6.3beta. actually says "b" in filename.
- 6.5: similar as 6.4
- 6.6: broken mapname says "6.4" although filename is 6.6.
- new tauren summoner.
- actually says "b" in filename.


# 6.8 edited by sellmomforcash

quality: poor
tooltips: maybe no change
design changes: poor

- 6.8: looks like edit by "sellmomforcash". Fuck-loads of ancient hydras, footmen, and archers added. global exp. shit edit.
